variable "access_key" {}

variable "secret_key" {}

variable "region" {
  default = "eu-central-1"
}

variable "vpc_cb" {
  default = "172.16.0.0/16"
}

variable "route_ingress_cb" {
  default = "0.0.0.0/0"
}

variable "ssh_allowed_cb" {
  default = "0.0.0.0/0"
}

variable "https_allowed_cb" {
  default = "0.0.0.0/0"
}

variable "main_subnet_cb" {
  default = "172.16.0.0/16"
}

variable "application" {
  default = "Jenkins"
}
