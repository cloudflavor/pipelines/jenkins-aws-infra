resource "aws_vpc" "vpc" {
  cidr_block           = "${var.vpc_cb}"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags {
    project = "${var.application}"
  }
}

resource "aws_subnet" "subnet" {
  vpc_id = "${aws_vpc.vpc.id}"

  cidr_block              = "${var.main_subnet_cb}"
  map_public_ip_on_launch = true

  tags {
    project = "${var.application}"
  }
}

resource "aws_route_table_association" "rta" {
  subnet_id      = "${aws_subnet.subnet.id}"
  route_table_id = "${aws_route_table.rt.id}"
}

resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name = "${var.application}"
  }
}

resource "aws_egress_only_internet_gateway" "egw" {
  vpc_id = "${aws_vpc.vpc.id}"
}

resource "aws_route_table" "rt" {
  vpc_id = "${aws_vpc.vpc.id}"
}

resource "aws_route" "ir" {
  route_table_id         = "${aws_route_table.rt.id}"
  gateway_id             = "${aws_internet_gateway.gw.id}"
  destination_cidr_block = "${var.route_ingress_cb}"
}

resource "aws_route" "er" {
  route_table_id              = "${aws_route_table.rt.id}"
  destination_ipv6_cidr_block = "::/0"
  egress_only_gateway_id      = "${aws_egress_only_internet_gateway.egw.id}"
}

resource "aws_security_group" "ssh_https_access" {
  vpc_id = "${aws_vpc.vpc.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "6"
    cidr_blocks = ["${var.ssh_allowed_cb}"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "6"
    cidr_blocks = ["${var.https_allowed_cb}"]
  }

  tags {
    project = "${var.application}"
  }
}
